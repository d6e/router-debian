#!/usr/bin/env python3      
# This goes at /opt
import json                                                                                                                                            
import sys

IF_WAN = "ens1p0"
IF_LAN = "br0"
                                                                                                                                                       
rules = {                                                                                                                                              
    "filter": [
        ":INPUT DROP [0:0]",
        ":FORWARD ACCEPT [0:0]",  # Need to find out the proper rules to change this to DROP. Downloads in ipv6 are breaking
        ":OUTPUT ACCEPT [0:0]",
        "-A INPUT -i lo -j ACCEPT", 
        "-A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT",
        "-A INPUT -p ipv6-icmp -m comment --comment \"ipv6 is more dependent on ICMP\" -j ACCEPT",
        "-A INPUT -s fe80::/10 -j ACCEPT", # allow link-local connections (not sure if should be bound to interface)
#        "-A FORWARD -m state --state RELATED,ESTABLISHED -m comment --comment \"allow inbound existing connections\" -j ACCEPT",
#        "-A FORWARD -i {lan} -o {wan} -m comment --comment \"Allow all Internet bound traffic\" -j ACCEPT".format(wan=IF_WAN, lan=IF_LAN),
#        "-A FORWARD -p ipv6-icmp -m comment --comment \"forward any ICMP traffic\" -j ACCEPT",
    ],                                                 
    "nat": []                                                                                                                                          
}

with open(sys.argv[1]) as f:
    config = json.load(f)
    public_if = config["public-if"]
    for extport, dest in config['port-forwards'].items():
        #rules['nat'].append("-A PREROUTING -i {} -p tcp -m tcp --dport {} -j DNAT --to-destination {}".format(public_if, extport, dest))
        #rules['nat'].append("-A PREROUTING -i {} -p udp -m udp --dport {} -j DNAT --to-destination {}".format(public_if, extport, dest))
        rules['filter'].append("-A FORWARD -i {} -p tcp -m tcp --dport {} -m state --state NEW -j ACCEPT".format(public_if, extport))
        rules['filter'].append("-A FORWARD -i {} -p udp -m udp --dport {} -m state --state NEW -j ACCEPT".format(public_if, extport))
    rules['filter'].append("-A INPUT -i {} -m state --state NEW -m udp -p udp --dport 546 -d fe80::/64 -j ACCEPT".format(public_if))  # for upstream mosh
    rules['filter'].append("-A INPUT -i {} -m state --state NEW -m tcp -p tcp --dport 5510 -j ACCEPT".format(public_if))  # for ssh
    rules['filter'].append("-A INPUT -i {} -p udp -m udp --dport 60001:60010 -j ACCEPT".format(public_if))      # for mosh
    rules['filter'].append("-A INPUT -i {} -j REJECT --reject-with icmp6-port-unreachable".format(public_if))

for t in ['filter', 'nat']:
    print("*{}".format(t))
    print("\n".join(rules[t]))
    print("COMMIT")

