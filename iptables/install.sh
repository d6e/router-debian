#!/bin/bash
cp ./port-forward.sh /etc/network/if-pre-up.d/port-forward
chmod 755 /etc/network/if-pre-up.d/port-forward
ls -l "$PWD/port-forward.py" /opt/port-forward.py
ls -l "$PWD/port-forward6.py" /opt/port-forward6.py
ls -l "$PWD/port-forwards.json" /etc/port-forwards.json
if [[ "$(run-parts --test /etc/network/if-pre-up.d)" =~ "port-forward" ]]; then
    echo "Successfully installed!"
else
    echo "Failure to install!"
fi 
