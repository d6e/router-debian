#!/bin/sh
# This goes at /etc/network/if-pre-up.d/iptables
set -ex
env | logger -t $0
/opt/port-forward.py /etc/port-forwards.json | /sbin/iptables-restore
/opt/port-forward6.py /etc/port-forwards.json | /sbin/ip6tables-restore
