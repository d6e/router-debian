enp1s0 -> the public interface (goes to modem)
enp2s0 -> the private interface (connects to switch)
enp3s0 -> unallocated (maybe use it for network monitoring?)

Install bridge utils so you have brctl for the /etc/network/interfaces bridge
```
apt install bridge-utils
wide-dhcpv6-server
```

Add a second network in `/etc/network/interfaces` for the new LAN:
```
# The external WAN interface
allow-hotplug enp1s0
iface enp1s0 inet dhcp

# The internal LAN interface
allow-hotplug enp2s0
iface enp2s0 inet static
   address 10.13.37.1
   netmask 255.255.255.0
   network 10.13.37.0
   broadcast 10.13.37.255
```

Install dnsmasq
```
apt install dnsmasq
```

Configure `/etc/dnsmasq.conf`:
```
interface=enp2s0
listen-address=127.0.0.1
domain=home.d6e.io
dhcp-range=10.13.37.50,10.13.37.254,12h
```

Configure `/etc/sysctl.conf`:
```
net.ipv4.ip_forward=1
net.ipv6.conf.all.forwarding=1
```

Install dependencies for Finn's iptables scripts
```
apt install python3
```

- Download finn's iptables scripts and install them. Make sure to make them executable!
